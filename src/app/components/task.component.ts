import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Task } from '../models/task.model';
import { TaskState } from '../constants/task-states';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html'
})

export class TaskComponent implements OnInit {
  title: string
  readonly TaskState = TaskState;

  @Input() task: Task;
  @Output() onPinTask: EventEmitter<any> = new EventEmitter()
  @Output() onArchiveTask: EventEmitter<any> = new EventEmitter()


  ngOnInit() { }

  onPin(id: any) {
    this.onPinTask.emit(id);
  }
  onArchive(id: any) {
    this.onArchiveTask.emit(id);
  }
}
